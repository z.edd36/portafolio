import React, { useState } from "react";
import Gallery from "./gallery/index";
import Graphics from "./graphics/index";
import "../../styles/demos/index.css";
import "../../styles/demos/responsiveIndex.css";
import ApiNasa from "./apiNasa/index";
import svg from "../../img/svg/index";

const Demos = ({ hookstateApp, hookSetStateApp }: any) => {
  const [stateDemos, setStateDemos] = useState<any>({
    indexSelected: 0,
    stateComponentsSelected: [
      <Gallery hookSetStateApp={hookSetStateApp} hookstateApp={hookstateApp} />,
    ],
  });

  const [stateListDemos] = useState([
    {
      name: "Imágenes",
      iconComponent: svg().iconGallery,
      components: [
        <Gallery
          hookSetStateApp={hookSetStateApp}
          hookstateApp={hookstateApp}
        />,
      ],
    },
    {
      name: "Conexion Api",
      iconComponent: svg().iconApi,
      components: [<ApiNasa />],
    },
    {
      name: "Graficos",
      iconComponent: "",
      components: [<Graphics />],
    },
  ]);

  return (
    <div className="divContainerDemos00">
      <div className="divContainerTitleDemos">
        <h1>Demos</h1>
      </div>
      <div className="divContainerSelectionDemos00">
        {stateListDemos.map((listDemo: any, index) => (
          <div
            className="divContainerSelectiondemos01"
            onClick={() => {
              setStateDemos({
                indexSelected: index,
                stateComponentsSelected: [listDemo.components],
              });
            }}
          >
            <h2
              style={
                stateDemos.indexSelected == index
                  ? {
                      color: "rgb(128, 114, 88)",
                      border: "solid 0.1rem rgb(78, 113, 141)",
                    }
                  : {}
              }
            >
              {listDemo.name}
            </h2>
            <div className="divContainerIconImg">{listDemo.iconComponent}</div>
          </div>
        ))}
      </div>
      <div className="divContainerDemos01">
        <section>{stateDemos.stateComponentsSelected}</section>
      </div>
    </div>
  );
};

export default Demos;
